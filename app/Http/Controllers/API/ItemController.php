<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ItemModel;
use App\Http\Traits\GeneralTrait;

class ItemController extends Controller
{
    use GeneralTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = ItemModel::select('*')->get();	
		if (!$getData->isEmpty()) {
			return $this->ResponseJson(200,"Item Data",$getData);
		}else{
			return $this->ResponseJson(404,"Item Not Found",array());
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'Required|string',
            'price' => 'Required',
            'cost' => 'Required'
        ];
        $ValidateData = $this->ValidateRequest($request->all(), $rules);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        $save = ItemModel::create($request->all());
        if(!$save){
            return $this->ResponseJson(406,"Server Error!");
        } 
        return $this->ResponseJson(200,"Item succesfully added",$save);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $getData = ItemModel::select('*')->where('id',$id)->first();	
		if (!empty($getData)) {
			return $this->ResponseJson(200,"Item Detail",$getData);
		}else{
			return $this->ResponseJson(404,"Item Not Found",$getData);
		}
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'id' => 'Required|integer',
            'name' => 'Required|string',
            'price' => 'Required',
            'cost' => 'Required'
        ];
        $ValidateData = $this->ValidateRequest($request->all(), $rules);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        $save = ItemModel::where('id',$request->id)->update($request->except(['_method','id']));
        if(!$save){
            return $this->ResponseJson(406,"Server Error!");
        } 
        return $this->ResponseJson(200,"Item succesfully updated",$save);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = ItemModel::where('id',$id)->delete();
        if(!$delete){
            return $this->ResponseJson(406,"Server Error!");
        } 
        return $this->ResponseJson(200,"Item succesfully deleted",$delete);
    }
}
