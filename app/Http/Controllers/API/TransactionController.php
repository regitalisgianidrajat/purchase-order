<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TrxPoHeaderModel;
use App\Models\TrxPoDetailModel;
use App\Models\ItemModel;
use App\Http\Traits\GeneralTrait;

class TransactionController extends Controller
{
    use GeneralTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = TrxPoHeaderModel::select('*')->with('OrderDetails')->get();	
		if (!$getData->isEmpty()) {
			return $this->ResponseJson(200,"Trx PO Data",$getData);
		}else{
			return $this->ResponseJson(404,"Trx PO Not Found",array());
		}
    }
    public function lastId()
    {
        $getData = TrxPoHeaderModel::select('id')->orderBy('id','desc')->first();	
		
        return $this->ResponseJson(200,"Trx PO Data",$getData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'po_number' => 'Required|string',
            'po_date' => 'Required|string'
        ];
        $ValidateData = $this->ValidateRequest($request->all(), $rules);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        if(count($request->po_item_id)>0){
            for ($i=0; $i < count($request->po_item_id); $i++) { 
                $getData = ItemModel::select('*')->where('id',$request->po_item_id[$i])->first();
                $dataItem['po_item_id'] = $request->po_item_id[$i];
                $dataItem['po_item_qyt'] = $request->po_item_qyt[$i];
                $dataItem['price_item'] = $getData['price']*$request->po_item_qyt[$i];
                $dataItem['cost_item'] = $getData['cost']*$request->po_item_qyt[$i];
                $itemDetail[] = $dataItem;
            }
        }
        $postHeader = array(
			'po_number' => $request->po_number,
			'po_date' => $request->po_date,
			'po_price_total' =>  array_sum(array_column($itemDetail,'price_item')),
			'po_cost_total' => array_sum(array_column($itemDetail,'cost_item')),
		);	
        $saveHeader = TrxPoHeaderModel::create($postHeader);
        if(!$saveHeader){
            return $this->ResponseJson(406,"Server Error!");
        }

        if(count($itemDetail)>0){
            for ($i=0; $i < count($itemDetail); $i++) { 
                $postDetail = array(
                    'po_h_id' => $saveHeader->id,
                    'po_item_id' => $itemDetail[$i]['po_item_id'],
                    'po_item_qyt' => $itemDetail[$i]['po_item_qyt'],
                    'po_item_price' => $itemDetail[$i]['price_item'],
                    'po_item_cost' => $itemDetail[$i]['cost_item'],
                );	
                $saveDetail = TrxPoDetailModel::create($postDetail);
            }
        }
        return $this->ResponseJson(200,"TRX PO succesfully created",$saveHeader);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $getData =  TrxPoHeaderModel::select('*')->with('OrderDetails')->where('id',$id)->first();	
		if (!empty($getData)) {
			return $this->ResponseJson(200,"Trx PO Detail",$getData);
		}else{
			return $this->ResponseJson(404,"Trx PO Found",$getData);
		}
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'po_h_id' => 'Required|integer'
        ];
        $ValidateData = $this->ValidateRequest($request->all(), $rules);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        if(count($request->po_item_id)>0){
            for ($i=0; $i < count($request->po_item_id); $i++) { 
                $getData = ItemModel::select('*')->where('id',$request->po_item_id[$i])->first();
                $dataItem['po_item_id'] = $request->po_item_id[$i];
                $dataItem['po_item_qyt'] = $request->po_item_qyt[$i];
                $dataItem['price_item'] = $getData['price']*$request->po_item_qyt[$i];
                $dataItem['cost_item'] = $getData['cost']*$request->po_item_qyt[$i];
                $itemDetail[] = $dataItem;
            }
        }
        $postHeader = array(
			'po_price_total' =>  array_sum(array_column($itemDetail,'price_item')),
			'po_cost_total' => array_sum(array_column($itemDetail,'cost_item')),
		);	
        $saveHeader = TrxPoHeaderModel::where('id',$request->po_h_id)->update($postHeader);
        if(count($itemDetail)>0){
            $deleteDetail = TrxPoDetailModel::where('po_h_id',$request->po_h_id)->delete();
            for ($i=0; $i < count($itemDetail); $i++) { 
                $postDetail = array(
                    'po_h_id' => $request->po_h_id,
                    'po_item_id' => $itemDetail[$i]['po_item_id'],
                    'po_item_qyt' => $itemDetail[$i]['po_item_qyt'],
                    'po_item_price' => $itemDetail[$i]['price_item'],
                    'po_item_cost' => $itemDetail[$i]['cost_item'],
                );	
                $saveDetail = TrxPoDetailModel::create($postDetail);
            }
        }
        return $this->ResponseJson(200,"Trx PO Detail succesfully updated",$saveHeader);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteHeader = TrxPoHeaderModel::where('id',$id)->delete();
        if(!$deleteHeader){
            return $this->ResponseJson(406,"Server Error!");
        } 
        
        $deleteDetail = TrxPoDetailModel::where('po_h_id',$id)->delete();
        return $this->ResponseJson(200,"Trx PO succesfully deleted",$deleteHeader);
    }
}
