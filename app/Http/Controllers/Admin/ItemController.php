<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use App\Http\Traits\GeneralTrait;

class ItemController extends Controller
{
    use GeneralTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['data']       = $this->GET(config('constants.url.api').'/api/item')['data'];
        $data['title']      = 'Manage Item';
        return view('admin.item.view', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title']      = 'Manage Data Item';
        return view('admin.item.add', $data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $general_data = $request->except(['_token']);
        $response = $this->POST(config('constants.url.api').'/api/item', $general_data);
        if ($response['status'] == true) {
            return Redirect::to('admin/item')->with('alert-success', $response['message']); 
        }else{
            return Redirect::back()->withErrors(['error', $response['message']]);
        }
  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['data']       = $this->GET(config('constants.url.api').'/api/item/'.$id)['data'];
        $data['title']      = 'Manage Item Detail';
        return view('admin.item.edit', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $general_data =$request->except(['_token']);
        $response = $this->PUT(config('constants.url.api').'/api/item', $general_data);
        if ($response['status'] == true) {
            return Redirect::to('admin/item')->with('alert-success', $response['message']);  
        }else{
            return Redirect::back()->withErrors(['error', $response['message']]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = $this->DELETE(config('constants.url.api').'/api/item/'.$id);
        
        if ($delete['status'] == true) {
            return Redirect::to('admin/item')->with('alert-success', $delete['message']);  
        }else{
            return Redirect::back()->withErrors(['error', $delete['message']]);
        }
    }
}
