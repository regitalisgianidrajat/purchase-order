<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use App\Http\Traits\GeneralTrait;

class TransactionController extends Controller
{
    use GeneralTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['item']       = $this->GET(config('constants.url.api').'/api/item')['data'];
        $data['data']       = $this->GET(config('constants.url.api').'/api/trx-po')['data'];
        $data['title']      = 'Manage Transaction';
        return view('admin.trx.view', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['item']       = $this->GET(config('constants.url.api').'/api/item')['data'];
        $data['id']         = $this->GET(config('constants.url.api').'/api/trx-po/lastid')['data'];
        $data['title']      = 'Manage Data Transaction';
        return view('admin.trx.add', $data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $general_data = $request->except(['_token']);
        $general_data['po_item_id'] = array_filter($general_data['po_item_id']);  
        $general_data['po_item_qyt'] = array_filter($general_data['po_item_qyt']);  
        $response = $this->POST(config('constants.url.api').'/api/trx-po', $general_data);
        if ($response['status'] == true) {
            return Redirect::to('admin/trx-po')->with('alert-success', $response['message']); 
        }else{
            return Redirect::back()->withErrors(['error', $response['message']]);
        }
  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['data']       = $this->GET(config('constants.url.api').'/api/trx-po/'.$id)['data'];
        $data['item']       = $this->GET(config('constants.url.api').'/api/item')['data'];
        $data['title']      = 'Manage Transaction';
        return view('admin.trx.edit', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $general_data =$request->except(['_token']);
        
        $general_data['po_item_id'] = array_filter($general_data['po_item_id']);  
        $general_data['po_item_qyt'] = array_filter($general_data['po_item_qyt']);  
        $response = $this->PUT(config('constants.url.api').'/api/trx-po', $general_data);
        if ($response['status'] == true) {
            return Redirect::to('admin/trx-po')->with('alert-success', $response['message']);  
        }else{
            return Redirect::back()->withErrors(['error', $response['message']]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = $this->DELETE(config('constants.url.api').'/api/trx-po/'.$id);
        
        if ($delete['status'] == true) {
            return Redirect::to('admin/trx-po')->with('alert-success', $delete['message']);  
        }else{
            return Redirect::back()->withErrors(['error', $delete['message']]);
        }
    }
}
