<?php

namespace App\Http\Traits;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;


trait GeneralTrait {


	private $_client;
	public function __construct(){
		$this->_client = new Client();
	 }
   
    public function ResponseJson($status,$message,$data = null){
		if($status == 200){
			$response = [
				'status' => true,
				'message' => $message,
				'data' => $data
			];
		}else{
			$response = [
				'status' => false,
				'message' => $message,
				'data' => $data
			];
		}
		return response()->json($response, 200);
	}
    function ValidateRequest($params,$rules){

		$validator = Validator::make($params, $rules);

		if ($validator->fails()) {
			$response = [
				'status' => false,
				'message' => $validator->messages()
			];
			return response()->json($response, 200);
		}
	}   
	protected function POST($url,$data = [],$headers = [],$timeout = ['connection_timeout' => 600,'timeout'=> 600]){
		return json_decode($this->_client->POST($url,[
			'form_params' => $data,
			'headers' => $headers,
			$timeout
		])->getBody(),true);
	  }
	
	protected function GET($url,$data = [],$headers = [],$timeout = ['connection_timeout' => 600,'timeout'=> 600]){
		$requestData = $this->_client->GET($url,[
			'form_params' => $data,
			'headers' => $headers,
			$timeout
		]);
		return json_decode($this->_client->GET($url,[
			'form_params' => $data,
			'headers' => $headers,
			$timeout
		])->getBody(),true);
	}
	protected function DELETE($url,$data = [],$headers = [],$timeout = ['connection_timeout' => 600,'timeout'=> 600]){
		return json_decode($this->_client->DELETE($url,[
			'form_params' => $data,
			'headers' => $headers,
			$timeout
		])->getBody(),true);
	}
	protected function PUT($url,$data = [],$headers = [],$timeout = ['connection_timeout' => 600,'timeout'=> 600]){
		return json_decode($this->_client->PUT($url,[
			'form_params' => $data,
			'headers' => $headers,
			$timeout
		])->getBody(),true);
	}
	
}