<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemModel extends Model
{
    use SoftDeletes;
    protected $table   = 'ms_item';
    public $primarykey = 'id';
    
    public $timestamps = true;

    protected $fillable = [
		'name',
		'price',
        'cost'
	];
		
	protected $hidden = [
		'created_at',
		'deleted_at',
		'updated_at'
    ];

    public function OrderDetail()
    {
        return $this->hasMany('App\Models\TrxPoDetailModel', 'po_item_id', 'id');
    }

}
