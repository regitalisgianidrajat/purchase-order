<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrxPoDetailModel extends Model
{
    protected $table   = 'trx_po_d';
    public $primarykey = 'id';
    
    public $timestamps = true;

    protected $fillable = [
		'po_h_id',
		'po_item_id',
        'po_item_qyt',
        'po_item_price',
        'po_item_cost'
	];
		
	protected $hidden = [
		'created_at',
		'updated_at'
    ];
    public function OrderHeader()
    {
        return $this->belongsTo('App\Models\TrxPoHeaderModel', 'id', 'po_h_id');
    }
    public function ItemDetail()
    {
        return $this->belongsTo('App\Models\ItemModel', 'id', 'po_item_id');
    }
}
