<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrxPoHeaderModel extends Model
{
    use SoftDeletes;
    protected $table   = 'trx_po_h';
    public $primarykey = 'id';
    
    public $timestamps = true;

    protected $fillable = [
		'po_number',
		'po_date',
        'po_price_total',
        'po_cost_total'
	];
		
	protected $hidden = [
		'created_at',
		'deleted_at',
		'updated_at'
    ];
    // protected $casts = [
    //     'po_date' => 'datetime'
    // ];

    public function OrderDetails()
    {
        return $this->hasMany('App\Models\TrxPoDetailModel', 'po_h_id', 'id');
    }
}
