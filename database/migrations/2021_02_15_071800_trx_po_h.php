<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TrxPoH extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_po_h', function (Blueprint $table) {
            $table->id();
            $table->integer('po_number');
			$table->dateTime('po_date')->nullable();
            $table->double('po_price_total', 20, 2);
            $table->double('po_cost_total', 20, 2);
			$table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_po_h');
    }
}
