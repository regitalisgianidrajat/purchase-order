<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TrxPoD extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_po_d', function (Blueprint $table) {
            $table->id();
            $table->integer('po_h_id');
            $table->integer('po_item_id');
            $table->integer('po_item_qyt');
            $table->double('po_item_price', 20, 2);
            $table->double('po_item_cost', 20, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_po_d');
    }
}
