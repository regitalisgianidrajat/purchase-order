@extends('template.main')
@section('content')
<style>
.btn-light{
    padding:15px;
}
</style>
    <h1 class="mt-4 mb-4" style="margin-bottom:0px!important">{{$title}}</h1>
    
    <ol class="breadcrumb" style="background-color:#fff">
          <li class="breadcrumb-item"><a href="{{ url('/user/') }}">Home</a></li>
          <li class="breadcrumb-item active">{{ $title }}</li>
        </ol>
    <hr>
    @if($errors->any())
        <div class="card-body notif-message">
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i> Alert! </h5>
            {{$errors->first()}}
            </div>
        </div>
    @endif
    <form action="{{ url('/admin/item') }}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">Name</label>
            <input class="form-control" type="text" name="name"placeholder="Name" required>
        </div>
        <div class="form-group">
            <label for="price">Price</label>
            <input class="form-control" type="number" name="price" id="price" placeholder="10000" required>
        </div>
        <div class="form-group">
            <label for="cost">Cost</label>
            <input class="form-control" type="number" name="cost" id="cost" placeholder="10000" required>
        </div>
        <div class="form-group float-right">
            <button class="btn btn-lg btn-danger" type="reset">Reset</button>
            <button class="btn btn-lg btn-primary" type="submit">Submit</button>
        </div>
        
    </form>

@endsection