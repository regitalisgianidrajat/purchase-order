@extends('template.main')
@section('content')
    <h1 class="mt-4 mb-4">{{$title}}
    <a class="btn btn-primary float-right mt-2" href="{{url('/admin/item/add')}}" role="button">Create Item</a></h2><hr>
    @if(Session::get('alert-success'))
        <div class="card-body notif-message">
            <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i> Alert! </h5>
            {{ Session::get('alert-success') }}
        </div>
        </div>
    @endif
    <table id="data_users_reguler" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Cost</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $row)
            <tr>
                    <td>{{ $row['name'] }}</td>
                    <td>{{ $row['price'] }}</td>
                    <td>{{ $row['cost']}}</td>
                    <td>
                        <a href="{{ url('/admin/item/edit/'.$row["id"])}}" class="btn btn-xs btn-primary">Edit</a> |
                        <a href="{{ url('/admin/item/'.$row["id"])}}"" class="btn btn-xs btn-danger" onclick="return confirm('Are you want to delete this data ?');">Delete</a>
                    </td>
                </tr>
            @endforeach
            
    </table>
@endsection