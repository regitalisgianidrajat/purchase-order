@extends('template.main')
@section('content')
<style>
.btn-light{
    padding:15px;
}
</style>
    <h1 class="mt-4 mb-4" style="margin-bottom:0px!important">{{$title}}</h1>
    
    <ol class="breadcrumb" style="background-color:#fff">
          <li class="breadcrumb-item"><a href="{{ url('/user/') }}">Home</a></li>
          <li class="breadcrumb-item active">{{ $title }}</li>
        </ol>
    <hr>
    @if($errors->any())
        <div class="card-body notif-message">
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i> Alert! </h5>
            {{$errors->first()}}
            </div>
        </div>
    @endif
    <form action="{{ url('/admin/trx-po ') }}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">PO Number</label>
            <input class="form-control" type="text" name="po_number" value ="<?php echo date('Ymd').'-'.str_pad($id + 1, 5, 0, STR_PAD_LEFT);?>" readonly>
        </div>
        <div class="form-group">
            <label for="name">Date</label>
            <input class="form-control" type="text" name="po_date" value ="<?php echo date('Y-m').'-'.str_pad($id + 1, 5, 0, STR_PAD_LEFT);?>" readonly >
        </div><br>
        <div class="card-header"><strong>Order Detail</strong></div><br><br>
        <div class="form-group row">
        <label class="col-md-3 col-form-label" for="textarea-input">Item</label>
            <div class="col-md-9">
            <select name="po_item_id[]" id="po_item_id" class="form-control" data-live-search="true" style="height:50px!important">
                <option value="">--- Select Item ---</option>
                @foreach ($item as $key)
                    <option value="{{ $key['id'] }}">{{  $key['name'] }}</option>
                @endforeach
            </select>
            </div>
        </div>  

        <div class="form-group row">
        <label class="col-md-3 col-form-label" for="textarea-input">Quantity</label>
            <div class="col-md-9">
                <input class="form-control" id="text-input" type="text" name="po_item_qyt[]">
            </div>
        </div>
        <?php 
            for($i=2; $i<100; $i++){ ?>
            <div class="row<?php echo $i ?>" style="display:none">
            <div class="form-group row">
        <label class="col-md-3 col-form-label" for="textarea-input">Item</label>
            <div class="col-md-9">
            <select name="po_item_id[]" id="po_item_id" class="form-control" data-live-search="true" style="height:50px!important">
                <option value="">--- Select Item ---</option>
                @foreach ($item as $key)
                    <option value="{{ $key['id'] }}">{{  $key['name'] }}</option>
                @endforeach
            </select>
            </div>
        </div>  

        <div class="form-group row">
        <label class="col-md-3 col-form-label" for="textarea-input">Quantity</label>
            <div class="col-md-9">
                <input class="form-control" id="text-input" type="text" name="po_item_qyt[]">
            </div>
        </div>
            </div>
            <?php }?>
        
        <div class="form-group row">
        <label class="col-md-3 col-form-label" for="textarea-input"></label>
            <div class="col-md-9">
                <button type="button" id="addRow" class="btn btn-sm btn-info" row="2">Add More Item</button>
                <button type="button"  class="btn mnc btn-sm btn-danger" id="cancel" style=" display: none;">&nbsp;Cancel</button>
            </div>
        </div>
        <br><br>
        <div class="form-group float-right">
            <button class="btn btn-lg btn-danger" type="reset">Reset</button>
            <button class="btn btn-lg btn-primary" type="submit">Submit</button>
        </div>
        
        
    </form>
<script>
    $(document).ready(function(){
	xx=1;
	$('#files').val(xx);
    $('#addRow').click(function(){
    	//alert (xx);
	    $(".mnc").fadeIn();
	    $(this).attr('disabled','disabled');
	    $(this).attr('disabled','disabled');
	    row = $(this).attr('row');
	   
                    
	    $('.row'+row).fadeIn();	
	    row++;
	    xx=xx+1;
	    //alert(xx);
	    if(row==10)
		{
            // alert("You can only add 3 rewards");
            $('#addRow').hide();
		}
	    $('#addRow').attr('row',row);
	    $('#addRow').removeAttr('disabled');
	    $('#files').val(xx);	
		    
    });
    $('#cancel').click(function(){
			row=$("#addRow").attr('row');
			//alert (row);
			row=row-1;
			xx=xx-1;
			
			
			$("input#files").val(xx);
			//$("input#admins1").val(x4);
			$('.row'+row).hide();
			if(row==2)
			{
				$(".mnc").hide();
			}
			$('#addRow').fadeIn();
            $('#addRow').removeAttr('disabled');
			$("#addRow").attr('row',row);
			
	});
        
});
</script>
@endsection