@extends('template.main')
@section('content')
    <h1 class="mt-4 mb-4">{{$title}}
    <a class="btn btn-primary float-right mt-2" href="{{url('/admin/trx-po/add')}}" role="button">Create Trancation</a></h2><hr>
    @if(Session::get('alert-success'))
        <div class="card-body notif-message">
            <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i> Alert! </h5>
            {{ Session::get('alert-success') }}
        </div>
        </div>
    @endif
    <table id="data_users_reguler" class="display" style="width:100%">
        <thead>
            <tr>
                <th>PO Number</th>
                <th>PO Date</th>
                <th>Price Total</th>
                <th>Cost Total</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $row)
            <tr>
                    <td>{{ $row['po_number'] }}</td>
                    <td>{{ $row['po_date'] }}</td>
                    <td>{{ $row['po_price_total']}}</td>
                    <td>{{ $row['po_cost_total']}}</td>
                    <td>
                        <a href="{{ url('/admin/trx-po/edit/'.$row["id"])}}" class="btn btn-xs btn-primary">Edit</a> |
                        <a href="{{ url('/admin/trx-po/delete/'.$row["id"])}}"" class="btn btn-xs btn-danger" onclick="return confirm('Are you want to delete this data ?');">Delete</a>
                    </td>
                </tr>
            @endforeach
            
    </table>
@endsection