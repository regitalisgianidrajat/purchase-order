<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// API
    //item
    Route::get('item','API\ItemController@index');
    Route::get('item/{id}','API\ItemController@show');
    Route::post('item','API\ItemController@store');
    Route::put('item','API\ItemController@update');
    Route::delete('item/{id}','API\ItemController@destroy');
    //trx
    Route::get('trx-po','API\TransactionController@index');
    Route::get('trx-po/lastid','API\TransactionController@lastId');
    Route::get('trx-po/{id}','API\TransactionController@show');
    Route::post('trx-po','API\TransactionController@store');
    Route::put('trx-po','API\TransactionController@update');
    Route::delete('trx-po/{id}','API\TransactionController@destroy');


