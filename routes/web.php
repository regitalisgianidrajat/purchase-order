<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin/item','Admin\ItemController@index');
Route::get('/admin/item/add','Admin\ItemController@create');
Route::get('/admin/item/edit/{id}','Admin\ItemController@show');
Route::post('/admin/item','Admin\ItemController@store');
Route::post('/admin/item/update','Admin\ItemController@update');
Route::get('/admin/item/{id}','Admin\ItemController@destroy');

Route::get('/admin/trx-po','Admin\TransactionController@index');
Route::get('/admin/trx-po/add','Admin\TransactionController@create');
Route::get('/admin/trx-po/edit/{id}','Admin\TransactionController@show');
Route::post('/admin/trx-po','Admin\TransactionController@store');
Route::post('/admin/trx-po/update','Admin\TransactionController@update');
Route::get('/admin/trx-po/delete/{id}','Admin\TransactionController@destroy');